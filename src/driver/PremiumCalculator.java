package driver;

import java.util.Scanner;

import model.Habit;
import model.HealthProperty;
import model.Person;

public class PremiumCalculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				
		Person person= getUserInput();
		
		Double totalPremium=computePremium(person);
		
		String msg="Health Insurance Premium for ";
		if(person.getGender().toLowerCase().equals("male")){
			msg=msg+"Mr. ";
		}else{
				msg=msg+"Mrs ";
		}
		msg+=person.getName()+" : Rs."+totalPremium.intValue();
		
		System.out.println(msg);
		
		
	}

	private static Person getUserInput() {
		
		Person person=new Person();
		
		Scanner scan=new Scanner(System.in);
		
		System.out.println("**Welcome to Health Insurance Premium Quote Generator App**");
		
		System.out.println("Please provide following information:");
		
		System.out.println("Name : ");
		
		String name = scan.nextLine();
		
		System.out.println("Gender : ");
		
		String gender = scan.nextLine();
		
		System.out.println("Age : ");
		
		int age = Integer.parseInt(scan.nextLine());
		
		person.setName(name);
		person.setGender(gender);
		person.setAge(age);
		
		System.out.println("Current health : ");
		
		System.out.println("Hypertension(Yes/No) : ");
		
		String hyper = scan.nextLine();
		
		System.out.println("Blood pressure(Yes/No) : ");
		
		String bp = scan.nextLine();
		
		System.out.println("Blood sugar(Yes/No) : ");
		
		String sugar = scan.nextLine();
		
		System.out.println("Overweight(Yes/No) : ");
		
		String overweight = scan.nextLine();
		
		HealthProperty property=new HealthProperty();
		
		property.setHyperTension(hyper.toLowerCase().equals("yes")?true:false);
		property.setBloodPressure(bp.toLowerCase().equals("yes")?true:false);
		property.setBloodSugar(sugar.toLowerCase().equals("yes")?true:false);
		property.setOverWeight(overweight.toLowerCase().equals("yes")?true:false);
		
		person.setCurrentHealth(property);
		
		
		System.out.println("Habits : ");
		
		System.out.println("Smoking(Yes/No) : ");
		
		String smoking = scan.nextLine();
		
		System.out.println("Alcohol(Yes/No) : ");
		
		String alcohol = scan.nextLine();
		
		System.out.println("Daily exercise(Yes/No) : ");
		
		String exercise = scan.nextLine();
		
		System.out.println("Drugs(Yes/No) : ");
		
		String drugs = scan.nextLine();
		
		Habit habit=new Habit();
		
		habit.setSmoking(smoking.toLowerCase().equals("yes")?true:false);
		habit.setAlcohol(alcohol.toLowerCase().equals("yes")?true:false);
		habit.setDailyExercise(exercise.toLowerCase().equals("yes")?true:false);
		habit.setDrugs(drugs.toLowerCase().equals("yes")?true:false);
		
		person.setHabit(habit);
		
		return person;
	}

	private static double computePremiumBasedOnAge(Person person,int base,double total){
		int age=person.getAge();
		if(age<18){
			return base;
		}
		else if(age <= 25){
			total=base+(base*0.1);
		}
		else if(age<=30){
			total=base+(base*0.1);
			total=total+(total*0.1);
		}
		else if(age<=35){
			total=base+(base*0.1);
			total=total+(total*0.1);
			total=total+(total*0.1);
		}
		else if(age<=40){
			total=base+(base*0.1);
			total=total+(total*0.1);
			total=total+(total*0.1);
			total=total+(total*0.1);
		}else{
			total=base+(base*0.1);
			total=total+(total*0.1);
			total=total+(total*0.1);
			total=total+(total*0.1);
			int diff=age-40;
			for(int i=0;i<diff/5;i++){
				total=total+(total*0.2);	
			}
		}
		return total;
	}
	
	private static double computePremiumBasedOnProperty(Person person,int base,double total){
		if(person.getCurrentHealth().isHyperTension()){
			total=total+(total*0.01);
		}
		if(person.getCurrentHealth().isBloodPressure()){
			total=total+(total*0.01);
		}
		if(person.getCurrentHealth().isBloodSugar()){
			total=total+(total*0.01);
		}
		if(person.getCurrentHealth().isOverWeight()){
			total=total+(total*0.01);
		}
		return total;
	}
	
	private static double computePremiumBasedOnGoodHabit(Person person,int base,double total){
		if(person.getHabit().isDailyExercise()){
			total=total-(total*0.03);
		}
		return total;
	}
	
	private static double computePremiumBasedOnBadHabit(Person person,int base,double total){
		if(person.getHabit().isAlcohol()){
			total=total+(total*0.03);
		}
		if(person.getHabit().isDrugs()){
			total=total+(total*0.03);
		}
		if(person.getHabit().isSmoking()){
			total=total+(total*0.03);
		}
		return total;
	}
	
	private static double computePremium(Person person){
		int basePremium=5000;
		
		double totalPremium=0.0d;
		
		totalPremium=computePremiumBasedOnAge(person,basePremium,totalPremium);
		
		
		if(person.getGender().toLowerCase().equals("male")){
			totalPremium=totalPremium +(basePremium*0.02);
		}
		
		
		totalPremium=computePremiumBasedOnProperty(person,basePremium,totalPremium);
		
		totalPremium=computePremiumBasedOnGoodHabit(person,basePremium,totalPremium);
		
		totalPremium=computePremiumBasedOnBadHabit(person,basePremium,totalPremium);
	
		return totalPremium;
		
	}
}
