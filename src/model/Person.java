package model;

public class Person {

	private String name;
	private String gender;
	private int age;
	private HealthProperty currentHealth;
	private Habit habit;
	private double premium;
	
	
	public double getPremium() {
		return premium;
	}
	public void setPremium(double premium) {
		this.premium = premium;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public HealthProperty getCurrentHealth() {
		return currentHealth;
	}
	public void setCurrentHealth(HealthProperty currentHealth) {
		this.currentHealth = currentHealth;
	}
	public Habit getHabit() {
		return habit;
	}
	public void setHabit(Habit habit) {
		this.habit = habit;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuffer sb=new StringBuffer();
		sb.append("\n\nPersonal Details:")
		.append(this.getName() + " : "+this.getGender() + " : "+this.getAge())
		.append("\n\nHealth Properties : ")
		.append("\nBP : "+(this.getCurrentHealth().isBloodPressure()?"Yes":"No"))
		.append("\nBS : "+(this.getCurrentHealth().isBloodSugar()?"Yes":"No"))
		.append("\nHT : "+(this.getCurrentHealth().isHyperTension()?"Yes":"No"))
		.append("\nOW : "+(this.getCurrentHealth().isOverWeight()?"Yes":"No"))
		.append("\n\nHabits : ")
		.append("\nSmoking : "+(this.getHabit().isSmoking()?"Yes":"No"))
		.append("\nAlcohol : "+(this.getHabit().isAlcohol()?"Yes":"No"))
		.append("\nExercise : "+(this.getHabit().isDailyExercise()?"Yes":"No"))
		.append("\nDrugs : "+(this.getHabit().isDrugs()?"Yes":"No"));
		return sb.toString();
	}
	
}
